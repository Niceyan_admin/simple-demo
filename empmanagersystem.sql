/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : empmanagersystem

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 24/08/2020 00:06:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `empno` int(4) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `ename` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `job` varchar(9) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工种',
  `mgr` int(4) NULL DEFAULT NULL COMMENT '上级编号',
  `hiredate` date NULL DEFAULT NULL COMMENT '入职日期',
  `sal` double(7, 0) NULL DEFAULT NULL COMMENT '薪水',
  `comm` double(7, 0) NULL DEFAULT NULL COMMENT '奖金',
  `deptno` int(2) NULL DEFAULT NULL COMMENT '部门',
  PRIMARY KEY (`empno`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7954 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES (7369, 'SMITH', 'CLERK', 7902, '2020-08-01', 2000, 20, 10);
INSERT INTO `emp` VALUES (7499, 'ALLEN', 'SALESMAN', 7698, '2020-08-02', 1600, 300, 30);
INSERT INTO `emp` VALUES (7521, 'WARD', 'SALESMAN', 7698, '2020-08-03', 1250, 500, 30);
INSERT INTO `emp` VALUES (7566, 'JONES', 'MANAGER', 7839, '2020-08-04', 2975, 500, 20);
INSERT INTO `emp` VALUES (7654, 'MARTIN', 'SALESMAN', 7698, '2020-08-05', 1250, 1400, 30);
INSERT INTO `emp` VALUES (7698, 'BLAKE', 'MANAGER', 7839, '2020-08-06', 2850, 1400, 30);
INSERT INTO `emp` VALUES (7782, 'CLARK', 'MANAGER', 7839, '2020-08-07', 2450, 0, 10);
INSERT INTO `emp` VALUES (7788, 'SCOTT', 'ANALYST', 7566, '2020-08-08', 3000, 0, 20);
INSERT INTO `emp` VALUES (7839, 'KING', 'PRESIDENT', 7566, '2020-08-09', 5000, 0, 10);
INSERT INTO `emp` VALUES (7844, 'TURNER', 'SALESMAN', 7698, '2020-08-10', 1500, 0, 30);
INSERT INTO `emp` VALUES (7876, 'ADAMS', 'CLERK', 7788, '2020-08-11', 1100, 0, 20);
INSERT INTO `emp` VALUES (7900, 'JAMES', 'CLERK', 7698, '2020-08-12', 950, 0, 30);
INSERT INTO `emp` VALUES (7902, 'FORD', 'ANALYST', 7566, '2020-08-13', 3000, 0, 20);
INSERT INTO `emp` VALUES (7950, '老王', 'clekc', 8, '2020-08-18', 10000, 10000, 10);
INSERT INTO `emp` VALUES (7953, '小刘', 'CLERK', 521, '2020-08-23', 2000, 2000, 10);
INSERT INTO `emp` VALUES (7954, '张三', 'CLERK', 521, '2020-08-24', 2000, 2000, 10);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态：0正常，1注销',
  `createtime` date NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 0, '2020-07-28');
INSERT INTO `user` VALUES (2, 'root', 'root', 0, '2020-07-28');
INSERT INTO `user` VALUES (3, 'th', '123', 0, '2020-07-28');

SET FOREIGN_KEY_CHECKS = 1;
