// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css';

import axios from 'axios'


import Moment from 'moment'//导入文件
Vue.prototype.$moment = Moment;//赋值使用
Vue.use(ElementUI)

Vue.config.productionTip = false

// 路由判断登录 根据路由配置文件的参数,进每个路由前都会执行
   router.beforeEach((to, from, next) => {
        if (sessionStorage.getItem('user')) {
            //在已登录的情况下访问登录页会重定向到首页
            if (to.path === '/login') {
                next({path: '/Home' })/*  */
              } else {
                  if (to.matched.length === 0) { //如果未匹配到路由
                      next({ path: '/Home'}) //则跳转主页面
                  } else {
                    next(); //如果匹配到正确跳转
                  }
              }
          } else {
            // 没有登录则访问任何页面都重定向到登录页
            if (to.path === '/login') { // 如果当前已经是在/login路径下，则直接next(),必须加该判断
              next();
            } else { //如果是其他界面的路径则重定向到/login，这里注意：如果当前已经是在/login路径下，在执行重定向到/login，会报错。
              next({path:'/login',query: {redirect: to.fullPath}}); // 将跳转的路由path作为参数，登录成功后跳转到该路由
            }
          }
      });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
