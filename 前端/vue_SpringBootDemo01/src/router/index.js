import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login'


// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Home',
      name: 'Home',
      meta:{
              requireAuth:true
            },
      component: () => import('@/views/Home.vue'),
      children: [{
          path: '/empInfo',
          component: () => import('@/views/EmpInfo')
        }
      ]
    }
  ]
  // ,base: '/dist/'
})
