import router from '@/router/index.js'
import Message from '@/utils/message.js'
import axios from 'axios'; // 引入axios
import QS from 'qs'; // 引入qs模块，用来序列化post类型的数据

// 根据不同环境更改不同baseUrl
// let baseUrl = '';
// if (process.env.NODE_ENV == 'development') {
//     baseUrl = '开发地址测试地址';
// } else if (process.env.NODE_ENV == 'production') {
//     baseUrl = '生产地址';
// }

axios.defaults.baseURL = '/api';
//设置请求超时
axios.defaults.timeout = 10000;
//post请求头的设置
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';

// 请求拦截器(我们给每个请求都加 token,这样就可以节省每个请求再一次次的复制粘贴代码。)
axios.interceptors.request.use(
	config => {
		// 每次发送请求之前判断sessionStorage中是否存在token        
		// 如果存在，则统一在http请求的header都加上token，这样后台根据token判断你的登录情况
		const token = sessionStorage.getItem('token');
		if (token) {
			config.headers.Authorization = token  // 这里对应后台Shiro取token的key(Authorization)
		}
		return config;
	},
	error => {
		return Promise.error(error);
	})

// 响应拦截器
var _this = this;
axios.interceptors.response.use(
	response => {
		// 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据 ,否则的话抛出错误
		var code = response.data.code;
		var mes = response.data.message;
		if (code === 200) {
			return Promise.resolve(response);
		} else {
			switch (code) {
				// 401: 未登录        
				case 401:
					Message.install(mes, 'info');
					// 清除token
					localStorage.removeItem('token');
					// 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面 
					setTimeout(() => {
						router.push({
							path: '/login',
							query: {
								redirect:router.currentRoute.path
							}
						});
					}, 10000);
					break;
			
					// 403 token过期,对用户进行提示
					// 清除本地token和清空vuex中token对象
					// 跳转登录页面                
				case 403:
					Message.install(mes, 'info');
					// 清除token
					localStorage.removeItem('token');
					// 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面 
					setTimeout(() => {
						router.push({
							path: '/login',
							query: {
								redirect:router.currentRoute.fullPath
							}
						});
					}, 1000);
					break;
			
					// 404请求不存在
				case 404:
					Message.install({
						content: '网络请求不存在',
						time: 10000,
						type: 'danger',
						hasClose: true,
					});
					break;
			
					// 其他错误，直接抛出错误提示
				default:
					Message.install({
						content: response.data.message,
						time: 10000,
						type: 'danger',
						hasClose: true,
					});
			}
			return Promise.reject(response);
			
		}
	},
	error => { // 就是服务端抛出非2xx开头的错误，就会进入这里来。
		if (error.response.data) {
			switch (error.response.status) {
				// 401: 未登录        
				case 401:
					_this.$router.push({
						path: '/login',
						query: {
							redirect: _this.$router.currentRoute.fullPath
						}
					});
					break;

					// 403 token过期,对用户进行提示
					// 清除本地token和清空vuex中token对象
					// 跳转登录页面                
				case 403:
					_this.$message('登录失效,请重新登录', 'info');
					// 清除token
					localStorage.removeItem('token');
					// 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面 
					setTimeout(() => {
						_this.$router.push({
							path: '/login',
							query: {
								redirect: _this.$router.currentRoute.fullPath
							}
						});
					}, 1000);
					break;

					// 404请求不存在
				case 404:
					_this.$message({
						content: '网络请求不存在',
						time: 10000,
						type: 'danger',
						hasClose: true,
					});
					break;

					// 其他错误，直接抛出错误提示
				default:
					_this.$message({
						content: error.response.data.message,
						time: 10000,
						type: 'danger',
						hasClose: true,
					});
			}
			return Promise.reject(error.response);
		}
	}
)

/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */

export function getData(url, params) {
	return new Promise((resolve, reject) => {
		axios.get(url, {
			params: params
		}).then(res => {
			resolve(res.data)  //res.data ==> ResultBody:{code:xxx,message:xxx,result:{xxxxxxxx}}
		}).catch(err => {
			reject(err)
		})
	})
}

/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 * post方法必须要使用对提交从参数对象进行序列化的操作,这里通过node的qs模块来序列化我们的参数
 * 没有序列化操作，后台是拿不到提交的数据的
 */
/**
 * axios.get()方法和axios.post()在提交数据时参数的书写方式是有区别的
 * get的第二个参数是一个对象{}，然后这个对象的params属性值是参数对象
 * post的第二个参数就是一个参数对象
 */
export function postData(url, params) {
	return new Promise((resolve, reject) => {
		axios.post(url, JSON.stringify(params))
			.then(res => {
				resolve(res.data)
			})
			.catch(err => {
				reject(err)
			})
	})
}

/**
 * postFormData方法，对应post请求，用来提交文件+数据
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function postFormData(url, params) {
	return new Promise((resolve, reject) => {
		axios({
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			transformRequest: [function(data) { // 在请求之前对data传参进行格式转换
				const formData = new FormData()
				Object.keys(data).forEach(key => {
					formData.append(key, data[key])
				})
				return formData
			}],
			url,
			method: 'post',
			data: params
		}).then(res => {
			resolve(res.data)
		}).catch(err => {
			reject(err)
		})
	})
}