import Vue from 'vue'
import MessageComponent from '@/components/alert/message.vue' //引入message组件

// 生成一个Vue的子类 同时这个子类也就是组件
const messageBox = Vue.extend(MessageComponent)

const Message = {}; //定义一个Message常量对象，便于导出，然后通过导入才能使用该对象。

Message.install = function (options, type) {
  if (options === undefined || options === null) {
    options = {
      content: ''
    }
  } else if (typeof options === 'string' || typeof options === 'number') {
    options = {
      content: options
    }
    if (type != undefined && options != null) {
      options.type = type;
    }
  }

 // 生成一个该子类的实例  使用$mount()给组件手动挂载参数，然后将组件插入页面中
  let instance = new messageBox({
    data: options
  }).$mount()

  document.body.appendChild(instance.$el) // 获取instance实例关联的DOM元素；

  Vue.nextTick(() => {  // 调用这个函数后，立即控制组件显示出来
    instance.visible = true
  })
  
	//Vue.nextTick 在修改数据之后立即使用这个方法，获取更新后的 DOM
	/* 
	vm.message = 'changed'

	//想要立即使用更新后的DOM。这样不行，因为设置message后DOM还没有更新
	console.log(vm.$el.textContent) // 并不会得到'changed'

	//这样可以，nextTick里面的代码会在DOM更新后执行
	Vue.nextTick(function(){
			console.log(vm.$el.textContent) //可以得到'changed'
	}) 
	*/
}

export default Message   // 导出Message对象,然后在main.js里面导入接收该对象