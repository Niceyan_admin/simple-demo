package cn.bdqn.mybatis.controller;

import cn.bdqn.mybatis.entity.Emp;
import cn.bdqn.mybatis.entity.ResultBody;
import cn.bdqn.mybatis.service.IEmpServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Classname CRUDController
 * @Description TODO
 * @Date 2020-08-12 12:00 addUser
 * @Created by
 */
@RestController
@RequestMapping(value = "/emp", method = { RequestMethod.GET, RequestMethod.POST })
public class EmpInfoController {

    @Autowired
    private IEmpServie empService;

    @RequestMapping("/add")
    @ResponseBody
    public ResultBody handleAddEmpInfo(@RequestBody Emp emp){
        int i = empService.addEmpInfo(emp);
        System.out.println("添加用户信息：\t"+i);
        return new ResultBody().success(i);
    }

    @RequestMapping("/read")
    @ResponseBody
    public ResultBody handleFindEmpInfo(){
        List<Emp> empList = empService.findListEmp();
        System.out.println("查询服务信息：");
        return new ResultBody().success(empList);
    }

    @RequestMapping("/modify")
    @ResponseBody
    public ResultBody handleModifyEmpInfo(@RequestBody Emp emp){
        System.out.println("emp:"+emp);
        int i = empService.modifyEmpInfo(emp);
        System.out.println("修改用户信息：\t"+i);
        return new ResultBody().success(i);
    }
//
    @RequestMapping("/del")
    @ResponseBody
    public ResultBody handleRemoveEmpInfo(@RequestBody Integer empno){
        System.out.println("empno:"+empno);
        int i = empService.removeEmpInfo(empno);
        System.out.println("删除用户信息：\t"+i);
        return new ResultBody().success(i);
    }


    @RequestMapping("/testhealth")
    @ResponseBody
    public ResultBody testHealth(){
        /**
         * 获取前台参数内容
         * 拼接参数
         * 校验路径
         */
        List<Emp> empList=empService.findListEmp();
        String url="";
        for (int i = 0; i < empList.size(); i++) {
            Emp s = (Emp)empList.get(i);
            String ip=s.getEname();
            String port=s.getMgr();
            String route=s.getJob();
            //System.out.println(s.getEname()+"  "+s.getMgr()+"  "+s.getJob());
            String realroute="http://"+ip+":"+port+route;
            System.out.println("检测接口路径：");
            System.out.println(realroute);
            url=realroute;
        }
        String parms="";
        String str = SendGET(url,parms);
        System.err.print(str);
        System.out.println("返回结果:");
        return new ResultBody().success(empList);
    }


    public static String SendGET(String url,String param){
        String result="";//访问返回结果
        BufferedReader read=null;//读取访问结果
        try {
            //创建url
            URL realurl=new URL(url+param);
            //打开连接
            URLConnection connection=realurl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            //建立连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段，获取到cookies等
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            read = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),"UTF-8"));
            String line;//循环读取
            while ((line = read.readLine()) != null) {
                result += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(read!=null){//关闭流
                try {
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }



}
