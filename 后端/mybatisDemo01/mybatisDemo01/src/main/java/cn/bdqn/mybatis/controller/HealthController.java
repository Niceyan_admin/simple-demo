package cn.bdqn.mybatis.controller;

import cn.bdqn.mybatis.entity.Code;
import cn.bdqn.mybatis.entity.Emp;
import cn.bdqn.mybatis.entity.ResultBody;
import cn.bdqn.mybatis.service.CodeServie;
import cn.bdqn.mybatis.service.IEmpServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/health", method = { RequestMethod.GET, RequestMethod.POST })
public class HealthController {
    @Autowired
    private CodeServie codeServie;

    @RequestMapping("/healthCheck")
    public ResultBody healthCheck(){
        List<Code> codeList = codeServie.findListCode();
        System.out.println("健康测试");
        return new ResultBody().success(codeList);
    }
}
