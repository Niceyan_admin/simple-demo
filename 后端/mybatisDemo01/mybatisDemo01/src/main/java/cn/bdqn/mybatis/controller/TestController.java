package cn.bdqn.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Classname TestController
 * @Description TODO
 * @Date 2020-08-20 12:06
 * @Created by Development Intern
 */
@Controller
public class TestController {

    @Autowired
    private DataSource dataSource;

    @RequestMapping("/testDruid")
    public @ResponseBody String testDruid() {
        String sql = "SELECT now()";
        String str = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = dataSource.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                str = rs.getString(1);
                System.out.println(str);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }
}
