package cn.bdqn.mybatis.controller;

import cn.bdqn.mybatis.entity.ResultBody;
import cn.bdqn.mybatis.entity.User;
import cn.bdqn.mybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname UserInfoController
 * @Description TODO
 * @Date 2020-08-22 16:58
 * @Created by Development Intern
 */

@RestController
@RequestMapping(value = "/user", method = { RequestMethod.GET, RequestMethod.POST })
public class UserInfoController {

    @Autowired
    private IUserService userService;

    @RequestMapping("/login")
    @ResponseBody
    public ResultBody handleLogin(User paramUser){
        User user = userService.findByUserid(paramUser);
        if (user==null){
            return new ResultBody().success("用户名或密码错误，请重新输入");
        }
        return new ResultBody().success(user);
    }
}
