package cn.bdqn.mybatis.entity;

import org.springframework.http.HttpStatus;

/**
 * @Classname ResultBody
 * @Description TODO
 * @Date 2020/2/21 15:23
 * @Created by x1c
 */

public class ResultBody {
	/**
	 * 响应代码
	 */
	private Integer code;

	/**
	 * 响应消息
	 */
	private String message;

	/**
	 * 响应结果
	 */
	private Object result;

	/**
	 * 成功
	 *
	 * @return
	 */
	public static ResultBody success() {
		return success(null);
	}

	/**
	 * 成功
	 * 
	 * @param data
	 * @return
	 */
	public static ResultBody success(Object data) {
		ResultBody rb = new ResultBody();
		rb.setCode(HttpStatus.OK.value());
		rb.setMessage(HttpStatus.OK.name());
		rb.setResult(data);
		return rb;
	}

	/**
	 * 成功
	 * 
	 * @param data
	 * @return
	 */
	public static ResultBody success(String message, Object data) {
		ResultBody rb = new ResultBody();
		rb.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		rb.setMessage(message);
		rb.setResult(data);
		return rb;
	}

	/**
	 * 失败
	 */
	public static ResultBody error(Integer code, String message) {
		ResultBody rb = new ResultBody();
		rb.setCode(code);
		rb.setMessage(message);
		rb.setResult(null);
		return rb;
	}

	/**
	 * 失败
	 */
	public static ResultBody error(String message) {
		ResultBody rb = new ResultBody();
		rb.setCode(-1);
		rb.setMessage(message);
		rb.setResult(null);
		return rb;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
