package cn.bdqn.mybatis.mapper;

import cn.bdqn.mybatis.entity.Code;
import cn.bdqn.mybatis.entity.Emp;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("CodeMapper")
public interface CodeMapper {

    public List<Code> selectListCode();

}
