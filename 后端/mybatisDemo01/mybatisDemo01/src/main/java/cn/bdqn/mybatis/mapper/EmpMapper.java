package cn.bdqn.mybatis.mapper;

import cn.bdqn.mybatis.entity.Emp;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Classname EmpMapper
 * @Description Dao层员工信息接口类
 * @Date 2020-08-12 11:46
 * @Created by
 */
@Repository("empMapper")
public interface EmpMapper {
    public int insertEmpInfo(Emp emp);

    public List<Emp> selectListEmp();

    public int updateEmpInfo(Emp emp);

    public List<Emp> selectById();

    public int deleteEmpInfo(Integer empno);
}
