package cn.bdqn.mybatis.mapper;

import cn.bdqn.mybatis.entity.User;
import org.springframework.stereotype.Repository;
@Repository("userMapper")
public interface UserMapper {

    /**
     * 判断用户是否存在
     * @param userid
     * @return
     */
    User selectByUserid(User user);
}