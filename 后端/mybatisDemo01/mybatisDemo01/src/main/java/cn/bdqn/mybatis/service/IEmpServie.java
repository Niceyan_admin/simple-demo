package cn.bdqn.mybatis.service;

import cn.bdqn.mybatis.entity.Emp;

import java.util.List;

/**
 * @Classname IEmpServie
 * @Description Service层员工信息实现类
 * @Date 2020-08-12 18:35
 * @Created by
 */
public interface IEmpServie {
    public int addEmpInfo(Emp emp);

    public List<Emp> findListEmp();

    public List<Emp> selectById();

    public int modifyEmpInfo(Emp emp);

    public int removeEmpInfo(Integer empno);
}
