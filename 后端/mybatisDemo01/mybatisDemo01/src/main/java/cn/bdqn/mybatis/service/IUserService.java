package cn.bdqn.mybatis.service;

import cn.bdqn.mybatis.entity.User;

/**
 * @Classname IUserService
 * @Description TODO
 * @Date 2020-08-22 16:54
 * @Created by Development Intern
 */
public interface IUserService {
    //判断用户是否存在
    User findByUserid(User paramUser);
}
