package cn.bdqn.mybatis.service.impl;

import cn.bdqn.mybatis.entity.Code;
import cn.bdqn.mybatis.entity.Emp;
import cn.bdqn.mybatis.mapper.CodeMapper;
import cn.bdqn.mybatis.mapper.EmpMapper;
import cn.bdqn.mybatis.service.CodeServie;
import cn.bdqn.mybatis.service.IEmpServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("CodeService")
public class CodeServiceImpl implements CodeServie {

    @Autowired
    private CodeMapper codeMapper;



    @Override
    public List<Code> findListCode() {
        List<Code> codeList = codeMapper.selectListCode();
        return codeList;
    }
}
