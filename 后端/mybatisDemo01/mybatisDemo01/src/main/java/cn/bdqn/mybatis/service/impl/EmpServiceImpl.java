package cn.bdqn.mybatis.service.impl;

import cn.bdqn.mybatis.entity.Emp;
import cn.bdqn.mybatis.mapper.EmpMapper;
import cn.bdqn.mybatis.service.IEmpServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Classname EmpService
 * @Description Service层员工信息接口类
 * @Date 2020-08-12 11:49
 * @Created by
 */
@Service("empService")
public class EmpServiceImpl implements IEmpServie {

    @Autowired
    private EmpMapper empMapper;

    public int addEmpInfo(Emp emp){
        int i = empMapper.insertEmpInfo(emp);
        return  i;
    }

    public List<Emp> findListEmp(){
        List<Emp> empList = empMapper.selectListEmp();
        return empList;
    }

    @Override
    public List<Emp> selectById() {
        return empMapper.selectById();
    }


    public int modifyEmpInfo(Emp emp){
        int i = empMapper.updateEmpInfo(emp);
        return  i;
    }

    public int removeEmpInfo(Integer empno){
        int i = empMapper.deleteEmpInfo(empno);
        return  i;
    }
}
