package cn.bdqn.mybatis.service.impl;

import cn.bdqn.mybatis.entity.User;
import cn.bdqn.mybatis.mapper.UserMapper;
import cn.bdqn.mybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Classname UserServiceImpl
 * @Description TODO
 * @Date 2020-08-22 16:55
 * @Created by Development Intern
 */
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * //判断用户是否存在
     * @param userid
     * @return
     */
    @Override
    public User findByUserid(User paramUser) {
        User user = userMapper.selectByUserid(paramUser);
        return user;
    }
}
