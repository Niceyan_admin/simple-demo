package cn.bdqn.mybatis.test;

import cn.bdqn.mybatis.entity.Emp;
import cn.bdqn.mybatis.service.IEmpServie;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @Classname TestApp
 * @Description 员工信息测试类
 * @Date 2020-08-12 18:33
 * @Created by
 */
//这里只写SpringBootTest这个注解; 如果是junit4的话, 就要加上@RunWith(SpringRunner.class)
//@ExtendWith(SpringExtension.class) //导入spring测试框架[2]
@SpringBootTest  //提供spring依赖注入
public class TestApp {

    @Autowired
    private IEmpServie empService;
    @Autowired
    private Emp emp;

    /**
     * 测试添加员工信息
     */
    @Test
    public void testAddEmpInfo(){
        emp=new Emp();
        emp.setEname("小汤");
        //emp.setSal(6500.0);
        int i = empService.addEmpInfo(emp);
        System.out.println("添加用户信息：\t"+i);
    }

    /**
     * 测试查询员工信息
     */
    @Test
    public void testFindEmpInfo(){
        List<Emp> empList = empService.findListEmp();
        System.out.println("查询用户信息：");
        //默认查看5条
        for(int i = 0 ; i < empList.size() ; i++) {
            System.out.println(empList.get(i));
        }
    }

    /**
     * 测试修改员工信息
     * 以员工编号为条件
     */
    @Test
    public void testModifyEmpInfo(){
        emp=new Emp();
        emp.setEmpno(7940);
        emp.setEname("老王");
        //emp.setSal(10000.0);
        int i = empService.modifyEmpInfo(emp);
        System.out.println("修改用户信息：\t"+i);
    }

    /**
     * 测试删除员工信息
     * 以员工编号为条件
     */
    @Test
    public void testRemoveEmpInfo(){
        int i = empService.removeEmpInfo(7939);
        System.out.println("删除用户信息：\t"+i);
    }
}
