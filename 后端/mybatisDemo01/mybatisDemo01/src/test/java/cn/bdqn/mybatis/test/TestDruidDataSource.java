package cn.bdqn.mybatis.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @Classname TestDataSource
 * @Description TODO
 * @Date 2020-08-16 11:17
 * @Created by Development Intern
 */
@SpringBootTest  //提供spring依赖注入
public class TestDruidDataSource {
    @Autowired
    private DataSource dataSource;

    @Test
    public void contextLoads() throws SQLException {
        System.out.println(dataSource.getClass());

        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        connection.close();
    }
}
